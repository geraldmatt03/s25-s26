// [SECTION] Creating a Basic Server Setup

// Node JS => will provide us with a "runtime environment (RTE)" that will allow us to execute our program/application.

// RTE (Runtime Environment) -> we are pertaining to an environment/system in which a program can be excuted

// TASK: Let's create a standard server setup using 'plain' NodeJS

// 1. Identify and prepare the components/ingredients that you would need in order to execute the task.
	
	// the main ingredient when creating a server using plain Node is : http (hypertext transfer protocol).
	// http -> is a 'built-in' module of Node JS that will allow us to establish a connectional connection and will make it possible to transfer data over HTTP.

	// you need to be able to get the components first, we will use the require() directive -> this is a function that will allow us to gather and acquire certain packages that we will use to build our application.
const http = require('http');
	// http will provide us with all the components needed to establish a server.

	// http contains the utility constructor called 'createServer()' -> which will allow us to create an HTTP server.

// 2. Identify and describe a location where the connection will happen in order to provide a proper medium for both parties (client and server), we will then bind the connection to the desired port number.

let port = 4000;

// 3. Inside the server constructor, insert a method in which we can use in order to describe the connection that was established. Identify the interaction between the client and the server and pass them down as the arguments of the method
http.createServer((request, response) => {

// 4. Bind/assign the connection to the address, the listen() is used to bind and listen to a specific port whenever its being accessed by the computer 
	
	// write() -> this will allow us to insert messages or input to our page
	response.write(`Server is on Port ${port}`);
	response.write('\n');
	response.write('Magandang Hapon Batch 165');
	// response.write('Make me appear to the page');
	response.end(); // end() will allow us to identify a point where the transmission of data will end.
}).listen(port);

console.log('Server is Running');

// NOTE: When creating a project, we should always think of more efficent ways in executing the most simple tasks

// TASK: Integrate the use of an NPM Project in Node JS.
	// 1. Initialize an NPM(Node Package Manager) into the local project.

		// 2 Ways to perform this task

		// 1. npm init
		// 2. npm init -y(yes)

		// package.json => 'The heart of any Node projects'. It records all important metadata about the project, (libraries, packages, function) that makes up the system or application

		// Now that we have integrated an NPM into our project, lets solve some issues we are facing in our local project.

			// issue: needing to restart the project whenever changes occur.
			
			// solution: using NPM, lets install a dependency that will allow to automatically fix (hotfix) changes in our app.
			// => nodemon package

			// Basic Skills using NPM
			// -> install package
				// 1st method: npm install <name of package>
				// 2nd method: npm i <name of package>

				// note: you can also install multiple packages at the same time
					// nodemon
					// express
					// bcrypt

			// -> uninstall package
				// 1st method: npm uninstall <name of package>
				// 2nd method: npm un <name of package>
		
		// nodemon utility => is a CLI utility tool, it's task is to wrap your node js app and watch for any changes in the file system and automatically restart/hotfix the process.

		// IF this is the first time your local machine would use a platform/technology, the machince would need to recognize the technology first

			// issue: command not found

			// solution: INSTALL IT ON A GLOBAL SCALE
			// 'GLOBAL' => the file system structure of the whole machine
			// npm install -g nodemon

		// After installing the new package, register it to the package.json file

		// NOTE: When changing the "start" script into an unconvetional keyword, use "npm run <keyword>" to execute npm.

		// nodemon -> auto hotfix for changes done in the project.
		// node -> plain runtime environment
